package com.atlassian.automation.addon;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.event.PluginEventListener;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.event.events.PluginDisabledEvent;
import com.atlassian.plugin.event.events.PluginEnabledEvent;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.pocketknife.api.lifecycle.modules.DynamicModuleDescriptorFactory;
import com.atlassian.pocketknife.api.lifecycle.modules.LoaderConfiguration;
import com.atlassian.pocketknife.api.lifecycle.modules.ModuleRegistrationHandle;
import org.springframework.beans.factory.DisposableBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.util.function.Predicate;

public class DynamicExtensionLoader implements DisposableBean {
    private static final String THIS_ADDON_KEY = "com.atlassian.automation.addon.automation-thirdparty-dyn-loading-sample";
    private static final String A4J_PLUGIN_KEY = "com.codebarrel.addons.automation";
    private static final String A4J_PLUGIN_NAME = "Automation for Jira";
    private static final Logger LOG = LoggerFactory.getLogger(DynamicExtensionLoader.class);
    private static final Predicate<Plugin> IS_A4J = plugin -> plugin != null
            && A4J_PLUGIN_KEY.equals(plugin.getKey())
            && A4J_PLUGIN_NAME.equals(plugin.getName());

    private final DynamicModuleDescriptorFactory moduleDescriptorFactory;
    private final PluginEventManager pluginEventManager;
    private final LoaderConfiguration customConfiguration;
    private ModuleRegistrationHandle registrationHandle;

    @Inject
    DynamicExtensionLoader(
            DynamicModuleDescriptorFactory dynamicModuleDescriptorFactory,
            @ComponentImport PluginAccessor pluginAccessor,
            @ComponentImport PluginEventManager pluginEventManager
    ) {
        this.moduleDescriptorFactory = dynamicModuleDescriptorFactory;
        this.pluginEventManager = pluginEventManager;
        final Plugin plugin = pluginAccessor.getPlugin(THIS_ADDON_KEY);
        this.customConfiguration = new LoaderConfiguration(plugin);
        this.customConfiguration.addPathsToAuxAtlassianPluginXMLs("/modules-loaded-on-demand.xml");
        this.pluginEventManager.register(this);

        initializeModules(this.customConfiguration);

        LOG.info("DynamicExtensionLoader created.");
    }

    @PluginEventListener
    public void onPluginEnabled(final PluginEnabledEvent event) {
        if (IS_A4J.test(event.getPlugin())) {
            initializeModules(this.customConfiguration);
        }
    }

    @PluginEventListener
    public void onPluginDisabled(final PluginDisabledEvent event) {
        if (IS_A4J.test(event.getPlugin())) {
            destroyModules();
        }
    }

    @Override
    public void destroy() throws Exception {
        this.pluginEventManager.unregister(this);
        destroyModules();
        LOG.info("DynamicExtensionLoader destroyed.");
    }

    private void initializeModules(final LoaderConfiguration customConfiguration) {
        if (this.registrationHandle != null) {
            this.registrationHandle.unregister();
        }
        this.registrationHandle = moduleDescriptorFactory.loadModules(customConfiguration);
    }

    private void destroyModules() {
        if (this.registrationHandle != null) {
            this.registrationHandle.unregister();
            this.registrationHandle = null;
        }
    }
}
