import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { ErrorMessage, Field } from '@atlaskit/form';
import Textfield from '@atlaskit/textfield';
import styled from 'styled-components';

const FormContainer = styled.div`
  padding-bottom:5px;
`;

export const ProjectConditionConfigForm = (props) => {
  const config = props.config;
  const componentErrors = window.CodeBarrel.Automation.componentErrors(config);
  const disabled = window.CodeBarrel.Automation.isReadOnly();

  return (
    <FormContainer>
      <Field
        name="nameRegex"
        label="Match project name (regex)"
        isDisabled={disabled}
        isInvalid={!!componentErrors.nameRegex}
      >
        {({ fieldProps }) => (
          <Fragment>
            <Textfield
              {...fieldProps}
              value={config.value.nameRegex}
              onChange={(e) => {
                window.CodeBarrel.Automation.updateComponentValue(config, { nameRegex: e.target.value });
              }}
              placeholder="Leave blank to match all"
            />
            {componentErrors.nameRegex ? <ErrorMessage>{componentErrors.nameRegex}</ErrorMessage> : null}
          </Fragment>)
        }
      </Field>
    </FormContainer>
  );
};

ProjectConditionConfigForm.propTypes = {
  config: PropTypes.object.isRequired,
};

export default ProjectConditionConfigForm;
