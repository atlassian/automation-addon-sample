# Automation for Jira addon Samples

[![Atlassian license](https://img.shields.io/badge/license-Apache%202.0-blue.svg?style=flat-square)](LICENSE)

## tl;dr

Checkout `automation-thirdparty-sample` for a full working example of how to write an Automation for Jira extension using the latest APIs.

## Content

This repository contains 2 modules:

* `automation-thirdparty-sample` - fully featured example of how to write a third party extension for Automation for Jira
* `automation-thirdparty-dyn-loading-sample` - fully featured example with demonstrated dynamic module loading supported by [Atlassian Pocket Knife] tool.
* `automation-legacy-action-sample` - implementation of the deprecated legacy API. **DO NOT USE**

[Atlassian Pocket Knife]: https://bitbucket.org/atlassian/atlassian-pocketknife-dynamic-modules/

The legacy module should no longer be followed. It uses an outdated extension mechanism and all future extensions should
follow the newer thirdparty example, which was introduced in Automation for Jira 6.0.0.

## Some history

In the beginning Atlassian provided the free Jira Automation Labs plugin for Jira.  This already provided an API to allow for third party vendors to provide extensions (additional actions). 

Then came Code Barrel's Automation for Jira, which was a brand new app providing simple automation for all! Eventually we talked to 
Atlassian about taking over maintenance of the dated Jira Automation Labs plugin and upgrading them to our new codebase.
 
As part of this we maintained the old Labs API, even though quite a bit of it didn't make sense any more or was confusing.

The `automation-legacy-action-sample` shows how vendors could implement the old legacy APIs in Automation for Jira.  This was 
quite limited however and third party vendors could only interact with issues and write actions (no conditions nor triggers).

Then more recently in July 2019 we decided to provide a more fully featured API. Some of our goals were:

* Provide third party vendors with the ability to write triggers & conditions as well
* Allow more control over the front-end (i.e. render the UI with a framework of your choosing rather than forcing React)
* Allow pluggable rule components to deal with more than just issues
* Provide access to smart-value rendering to third party apps

Thus the Automation Third Party API v3.0.0 was born, which shipped in Automation for Jira 6.0.0.  The newer 
`automation-thirdparty-sample` contains a fully functional example of how to implement an app using this new API.  It's what
all future extensions for Automation for Jira should be written in.

We'll continue to support the older legacy API for quite some time, but we highly encourage all existing extensions to move on over to the newer API.

Fast forward to 2020 and we are now owned by Atlassian again, since Code Barrel was acquired by Atlassian late lat year!

## License

Copyright (c) 2020 Atlassian and others.
Apache 2.0 licensed, see [LICENSE](LICENSE) file.

[![With â¤ï¸ from Atlassian](https://raw.githubusercontent.com/atlassian-internal/oss-assets/master/banner-cheers.png)](https://www.atlassian.com)