import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { ErrorMessage, Field } from '@atlaskit/form';
import Textarea from '@atlaskit/textarea';
import styled from 'styled-components';

const FormContainer = styled.div`
  padding-bottom:5px;
`;

export const ProjectEditConfigForm = (props) => {
  const config = props.config;
  const componentErrors = window.CodeBarrel.Automation.componentErrors(config);
  const disabled = window.CodeBarrel.Automation.isReadOnly();

  return (
    <FormContainer>
      <Field
        name="projectDescription"
        label="Description"
        isDisabled={disabled}
        isInvalid={!!componentErrors.projectDescription}
      >
        {({ fieldProps }) => (
          <Fragment>
            <Textarea
              {...fieldProps}
              value={config.value.projectDescription}
              onChange={(e) => {
                window.CodeBarrel.Automation.updateComponentValue(config, { projectDescription: e.target.value });
              }}
              placeholder="Enter a new project description (including smart-values)"
              minimumRows={5}
            />
            {componentErrors.projectDescription ?
              <ErrorMessage>{componentErrors.projectDescription}</ErrorMessage> : null}
          </Fragment>)
        }
      </Field>
    </FormContainer>
  );
};

ProjectEditConfigForm.propTypes = {
  config: PropTypes.object.isRequired,
};

export default ProjectEditConfigForm;
