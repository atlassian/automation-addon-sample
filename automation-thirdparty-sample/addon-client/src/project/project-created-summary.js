import React from 'react';
import PropTypes from 'prop-types';

export const ProjectCreatedSummary = (props) => {
  const config = props.config.value;
  return (
    <span>
      {config.projectType ?
        AJS.I18n.getText('com.codebarrel.thirdparty.addon.project.created.type.specific', config.projectType) :
        AJS.I18n.getText('com.codebarrel.thirdparty.addon.project.created.type.all')}
    </span>
  );
};

ProjectCreatedSummary.propTypes = {
  config: PropTypes.object.isRequired,
};

export default ProjectCreatedSummary;
