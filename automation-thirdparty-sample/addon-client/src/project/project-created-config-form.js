import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { ErrorMessage, Field } from '@atlaskit/form';
import Select from '@atlaskit/select/Select';
import styled from 'styled-components';

const PROJECT_TYPE_OPTIONS = [
  { label: 'Business', value: 'business' },
  { label: 'Software', value: 'software' },
  { label: 'Service Desk', value: 'servicedesk' },
];

const FormContainer = styled.div`
  padding-bottom:5px;
`;

export const ProjectCreatedConfigForm = (props) => {
  const config = props.config;
  const componentErrors = window.CodeBarrel.Automation.componentErrors(config);
  const disabled = window.CodeBarrel.Automation.isReadOnly();

  // Example of how to access base Url.  Context also contains info about the user's permissions, available fields and
  // if we are in project (including the current project) or global admin.
  const context = window.CodeBarrel.Automation.getContext();
  const jiraBaseUrl = context.baseUrl;

  return (
    <FormContainer>
      <Field
        name="projectType"
        label={AJS.I18n.getText("com.codebarrel.thirdparty.addon.project.created.filter")}
        isDisabled={disabled}
        isInvalid={!!componentErrors.projectType}
      >
        {({ fieldProps }) => (
          <Fragment>
            <Select
              {...fieldProps}
              options={PROJECT_TYPE_OPTIONS}
              onChange={(newType) => {
                const newConfig = newType ? newType.value : null;
                window.CodeBarrel.Automation.updateComponentValue(config, { projectType: newConfig });
              }}
              value={PROJECT_TYPE_OPTIONS.find(type => type.value === config.value.projectType)}
              placeholder="Limit to certain project types or leave blank for all project types"
              validationState={!!componentErrors.projectType}
              isClearable
            />
            {componentErrors.projectType ? <ErrorMessage>{componentErrors.projectType}</ErrorMessage> : null}
          </Fragment>)
        }
      </Field>
    </FormContainer>
  );
};

ProjectCreatedConfigForm.propTypes = {
  config: PropTypes.object.isRequired,
};

export default ProjectCreatedConfigForm;
