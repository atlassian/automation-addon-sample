# Automation for Jira extension client developer guide

The old Atlassian Labs '[Jira Automation](bitbucket.org/atlassianlabs/automation/)' plugin used to render the UI on the server.

In [Automation for Jira](https://marketplace.atlassian.com/plugins/com.codebarrel.addons.automation/) this model is changing.  To provide a richer and easier to use experience for our users,
we render the configuration UI entirely on the client-side using React.  Automation for Jira provides a public client-side API 
that third party vendors can use to register their config UI components with Automation for Jira.

We build these components using the  latest tools of the trade:

* Yarn for JS package management
* [Atlaskit](https://aui-cdn.atlassian.com/atlaskit/registry/) UI components provided by Atlassian
* React
* Webpack for packaging everything up

The fully package JS files are then included in the atlassian plugin JAR using a standard web-resource
module descriptor in the `atlassian-plugin.xml`.  This has to be added to the `<context>automation-addons-context</context>`
for inclusion in the Automation for Jira pages.

## Anatomy of the client-side UI of an extension

There's a number of components you need to consider when implementing an Automation for Jira UI:

* The main component definition (see `sample-comment.js`): This defines the type of rule component, initial parameter values, its renderer and parameter validation
* The config component (see `sample-comment-config.js`): This renders the configuration UI on the right hand side

Depending on how complex your UI is, the main component definition could be split into further components.  For example 
you might want to introduce another component to render the 'summary' view.

For add-ons, configuration values we store have to be of the form:

```json
{
  "parameters": {
    "commentBody": [
      "Hello world"
    ],
    "securityLevel": []
  }
}
```
A JSON object with a top-level `parameters` field, which is a JSON object that can
contain any number of keys.  These keys *have* to point to arrays of values.  This is mainly so we can translate
this configuration into the Java representation on the server that's passed to the Action SPI (a `Map<String, List<String>>`). 

Please also note that the client-side UI renders inside an iframe (both in Server and Cloud). This means when you include links you need to add these 
link attributes so they open in the parent window: `target="_blank" rel="noopener noreferrer"`


## Public client-side API provided by Automation for Jira 

We provide the following public API in the `CodeBarrel` global namespace:

```javascript
CodeBarrel.Automation = {
  /**
   * Registers the provided component for rendering by Automation for Jira.  The provided component's type attribute
   * needs to match the <automation-action/> complete module key from the server.
   *
   * @param component the full component details to register.
   */
  registerComponent: (component) => {},
  /**
   * Updates the provided's component with new configuration parameters.  Note that this does not persist to the server
   * but instead will store this new value simply client side.  Only publishing the Automation rule persists the state to
   * the server.
   *
   * For pluggable components, the value has to be of the form: { parameters: { key1: [value(s)], key2: [value(s)] } }
   *
   * @param component the component to update
   * @param newValue the new configuration value to store
   */
  updateComponentValue: (component, newValue) => { },
  /**
   * Helper to wrap the provided child elements for this component in a configuration form, that will render the standard
   * configuration form header and buttons to save/cancel etc.
   *
   * @param component the current component
   * @param children the form contents to render
   * @return A React component for rendering
   */
  componentConfigForm: (component, children) => { },
  /**
   * Returns all validation errors that are relevant to the current component.
   *
   * @param component the current component
   */
  componentErrors: (component) => { },
  /**
   * Returns the current context.  This object contains baseUrl (for REST calls), user permission details, current project details and
   * if we are rendering the project or global admin UI. It also contains all configured fields in Jira.
   */
  getContext: () => { },
  /**
   * Returns if we are currently in a read only state (e.g. if the rule is in progress of being published to the server)
   */
  isReadOnly: () => { },
};
```

## Lifecycle

As soon as the component is defined, it needs to register itself using the `window.CodeBarrel.Automation.registerComponent(SampleCommentComponent);` 
public API.  This lets Automation for Jira know that your providing this pluggable UI linked to the `type` attribute
you specified in your component (which links to the server side `<automation-action/>`).

As soon as users update UI fields (e.g. type a character in an input field), you should trigger `updateComponentValue()`
with the new config values.  This will cause a re-render of the React component so you can provide live previews in the 
summary view on the left hand side in the rule sidebar.

When users hit 'Save' a call to your `validate()` (see `sample-comment.js`) method will be made giving you a chance to perform client-side validation.

To access errors for your component you can use `componentErrors()`.  

When users hit 'Publish rule' we take care of saving the latest state (last update provided by `updateComponentValue()`) to the server.
If there were validation errors on the server, then the config UI will be asked to re-render.  Server side errors for your
component are also accessible via `componentErrors()`.
