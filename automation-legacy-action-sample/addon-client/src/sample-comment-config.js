import React from 'react';
import PropTypes from 'prop-types';
import FieldTextArea from '@atlaskit/field-text-area';
import { ToggleStateless as Toggle } from '@atlaskit/toggle';
import { Label } from '@atlaskit/field-base';

export const SampleCommentConfigForm = (props) => {
  const component = props.component;
  const componentErrors = window.CodeBarrel.Automation.componentErrors(component);
  const disabled = window.CodeBarrel.Automation.isReadOnly();

  // Example of how to access base Url.  Context also contains info about the user's permissions, available fields and
  // if we are in project (including the current project) or global admin.
  const context = window.CodeBarrel.Automation.getContext();
  const jiraBaseUrl = context.baseUrl;
  console.log(jiraBaseUrl);

  const sendNotification = component.value.parameters.sendNotification[0] === 'true';

  return window.CodeBarrel.Automation.componentConfigForm(component,
    (<div>
      <p>{AJS.I18n.getText('com.codebarrel.addon.comment.config.desc')}</p>
      <FieldTextArea
        shouldFitContainer
        type="textarea"
        disabled={disabled}
        placeholder={AJS.I18n.getText('com.codebarrel.addon.comment.config.placeholder')}
        isInvalid={!!componentErrors.commentBody}
        invalidMessage={componentErrors.commentBody}
        id="commentBody"
        name="commentBody"
        label={AJS.I18n.getText('com.codebarrel.addon.comment.config.label')}
        autoFocus
        required
        value={component.value.parameters.commentBody[0]}
        onChange={(e) => window.CodeBarrel.Automation.updateComponentValue(component, {
          ...component.value,
          parameters: { ...component.value.parameters, commentBody: [e.target.value] }
        })}
      />
      <div>
        <Label label={AJS.I18n.getText('com.codebarrel.addon.notification.label')} />
        <Toggle
          isChecked={sendNotification}
          onChange={(e) => window.CodeBarrel.Automation.updateComponentValue(component, {
            ...component.value,
            parameters: { ...component.value.parameters, sendNotification: [!sendNotification] }
          })}
        />
      </div>
    </div>)
  );
};

SampleCommentConfigForm.propTypes = {
  component: PropTypes.object.isRequired,
};

export default SampleCommentConfigForm;
